$(document).ready(function() {
    // compromisos section
    $('#indicatorContainer, #indicatorContainer-2, #indicatorContainer-3, #indicatorContainer-4').radialIndicator({
        barColor: '#34ca70',
        barWidth: 9,
        initValue: 40,
        roundCorner: true,
        percentage: true
    });
    var radialObj = $('#indicatorContainer').data('radialIndicator');
    radialObj.animate(100);

    var radialObj = $('#indicatorContainer-2').data('radialIndicator');
    radialObj.animate(75);

    var radialObj = $('#indicatorContainer-3').data('radialIndicator');
    radialObj.animate(62);

    var radialObj = $('#indicatorContainer-4').data('radialIndicator');
    radialObj.animate(62);

    // report section
    $('#example').DataTable({
        responsive: true
            // fixedHeader: true,
            // "paging": false,
            // "order": [
            //     [3, "desc"]
            // ],
            // "ordering": true,
            // "info": false,
            // "searching": false,
            // "rowReorder": true,
            // responsive: true,
            // responsive: {
            //     details: false
            // }
    });
});